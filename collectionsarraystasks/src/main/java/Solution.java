
import java.util.*;

public class Solution {

    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
//        int[][] arr = new int[6][6];

        int[][] arr = {
                {1, 1, 1, 0, 0, 0},
                {0, 1, 0, 0, 0, 0,},
                {1, 1, 1, 0, 0, 0},
                {0, 0, 2, 4, 4, 0},
                {0, 0, 0, 2, 0, 0},
                {0, 0, 1, 2, 4, 0},
        };
        System.out.println(maxHourGlassInArray(arr));
    }

    public static int sumElementsArrayStartingWithIndexIj(int i, int j, int array[][]) {
        int sum = 0;
        for (int k = i; k < i + 3; k++) {
            for (int z = j; z < j + 3; z++) {
                sum += array[k][z];
            }
        }
        return sum;
    }

    public static int maxHourGlassInArray(int[][] array) {
        int max = sumElementsArrayStartingWithIndexIj(0, 0, array);
        int sum = 0;
        for (int i = 0; i < 6; i += 3) {
            for (int j = 0; j <= 3; j++) {
                sum = sumElementsArrayStartingWithIndexIj(i, j, array);
                if (sum > max) {
                    max = sum;
                }
            }
        }
        return max;
    }


}


