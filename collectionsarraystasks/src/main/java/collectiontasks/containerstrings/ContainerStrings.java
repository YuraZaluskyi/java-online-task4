package collectiontasks.containerstrings;

import java.util.Arrays;

public class ContainerStrings {
    private String[] containerStrings = new String[10];
    private int indexLastFreeCell = 0;

    public ContainerStrings() {
    }

    public int getIndexLastFreeCell() {
        return indexLastFreeCell;
    }

    public void addStringToContainer(String string) {
        if (indexLastFreeCell == containerStrings.length) {
            increaseArrayByHalf();
        } else {
            containerStrings[indexLastFreeCell] = string;
            indexLastFreeCell++;
        }
    }

    public String getStringFromContainerByIndex(int index) {
        return containerStrings[index];
    }

    private void increaseArrayByHalf() {
        String[] arrayString = new String[containerStrings.length + (containerStrings.length / 2)];
        for (int i = 0; i < containerStrings.length; i++) {
            arrayString[i] = containerStrings[i];
        }
        containerStrings = arrayString;
    }

    @Override
    public String toString() {
        return "ContainerStrings{" +
                "containerStrings=" + Arrays.toString(containerStrings) +
                '}';
    }
}
