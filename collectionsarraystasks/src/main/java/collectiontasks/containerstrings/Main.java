package collectiontasks.containerstrings;
public class Main {
    public static void main(String[] args) {
        ContainerStrings containerStrings = new ContainerStrings();
        containerStrings.addStringToContainer("0");
        containerStrings.addStringToContainer("1");
        containerStrings.addStringToContainer("2");
        containerStrings.addStringToContainer("3");
        containerStrings.addStringToContainer("4");
        containerStrings.addStringToContainer("5");
        containerStrings.addStringToContainer("6");
        containerStrings.addStringToContainer("7");
        containerStrings.addStringToContainer("8");
        containerStrings.addStringToContainer("9");
        containerStrings.addStringToContainer("10");
        containerStrings.addStringToContainer("11");
        containerStrings.addStringToContainer("12");
        containerStrings.addStringToContainer("13");
        containerStrings.addStringToContainer("14");
        containerStrings.addStringToContainer("15");
        System.out.println(containerStrings.getIndexLastFreeCell());
        System.out.println(containerStrings.getStringFromContainerByIndex(15));
        System.out.println(containerStrings);
    }
}
