package collectiontasks.classcontainingtwostring;

public class CountryCapital implements Comparable<CountryCapital> {
    private String country;
    private String capital;

    public CountryCapital() {
    }

    public CountryCapital(String country, String capital) {
        this.country = country;
        this.capital = capital;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCapital() {
        return capital;
    }

    public void setCapital(String capital) {
        this.capital = capital;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CountryCapital that = (CountryCapital) o;

        if (country != null ? !country.equals(that.country) : that.country != null) return false;
        return capital != null ? capital.equals(that.capital) : that.capital == null;
    }

    @Override
    public int hashCode() {
        int result = country != null ? country.hashCode() : 0;
        result = 31 * result + (capital != null ? capital.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "CountryCapital{" +
                "country='" + country + '\'' +
                ", capital='" + capital + '\'' +
                '}';
    }


    @Override
    public int compareTo(CountryCapital o) {
        return country.compareTo(o.country);
    }
}
