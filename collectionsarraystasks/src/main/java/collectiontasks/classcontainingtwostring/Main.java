package collectiontasks.classcontainingtwostring;

import java.util.*;

public class Main {
    public static void main(String[] args) {

        ArrayList<CountryCapital> listCountry = new ArrayList<>();
        listCountry.add(new CountryCapital("aaa", "bbb"));
        listCountry.add(new CountryCapital("ppp", "vvv"));
        listCountry.add(new CountryCapital("bbb", "sss"));
        listCountry.add(new CountryCapital("ttt", "www"));
        listCountry.add(new CountryCapital("xxx","fff"));
        listCountry.add(new CountryCapital("ggg","mmm"));
        listCountry.add(new CountryCapital("rrr","kkk"));
        listCountry.add(new CountryCapital("abb", "fgg"));
        listCountry.add(new CountryCapital("pty","aaa"));

        Collections.sort(listCountry);

//        listCountry.sort(new CountryCapitalComparator());
//        Collections.sort(listCountry, new CountryCapitalComparator());
        System.out.println(listCountry);

    }
}
