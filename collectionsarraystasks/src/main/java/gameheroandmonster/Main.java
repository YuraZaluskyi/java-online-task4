package gameheroandmonster;

import static gameheroandmonster.SomethingThatWaitsHero.getRandomNumber;

/*
* Герой комп'ютерної гри, що володіє силою в 25 балів, знаходиться в круглому
залі, з якого ведуть 10 закритих дверей. За кожними дверима героя чекає або
магічний артефакт, що дарує силу від 10 до 80 балів, або монстр, який має
силу від 5 до 100 балів, з яким герою потрібно битися. Битву виграє персонаж,
що володіє найбільшою силою; якщо сили рівні, перемагає герой.
✓ Організувати введення інформації про те, що знаходиться за дверима, або
заповнити її, використовуючи генератор випадкових чисел.
✓ Вивести цю саму інформацію на екран в зрозумілому табличному вигляді.
✓ Порахувати, за скількома дверима героя чекає смерть. Рекурсивно.
✓ Вивести номери дверей в тому порядку, в якому слід їх відкривати герою,
щоб залишитися в живих, якщо таке можливо.
* */

public class Main {
    public static Arena arena = new Arena();
    public static Hero hero = new Hero();

    public static void main(String[] args) {

        informationAboutArena();
        System.out.println("");
        System.out.println("                    T H E    G A M E    H A S   S T A R T E D !");
        System.out.println("");
        battleOnArena();


    }

    public static void battleOnArena() {
        for (int i = 0; i < arena.getQUANTITY_DOORS(); i++) {
            int randomDoor = getRandomNumber(0, (arena.getListSomethingThatWaitsHero().size() - 1));
            int randomDoorId = arena.getListSomethingThatWaitsHero().get(randomDoor).getId();
            SomethingThatWaitsHero something = arena.getSomethingThatWaitsHeroById(randomDoorId);
            System.out.print("The Hero opened the DOOR - " + randomDoorId + "  There is - " + something.getName() + "!");
            if (!isHeroWon(arena, hero, randomDoor)) {
                System.out.println("   HERO HAS NOT WON," + "  POWER - " + hero.getPower());
                System.out.println("");
                System.out.println("");
                System.out.println("      HERO HAS NOT WON !");
                System.out.println("");
                System.out.println("T H E    G A M E    O V E R !");
                return;
            }
            System.out.println("   HERO HAS WON," + "  POWER - " + hero.getPower());
            System.out.println("");


        }
        System.out.println("");
        System.out.println("       HERO HAS WON !");
        System.out.println("");
        System.out.println("T H E    G A M E    O V E R !");
        System.out.println("");
    }

    public static boolean isHeroWon(Arena arena, Hero hero, int randomDoor) {
        arena.setQuantityDoorsAlreadyOpen(arena.getQuantityDoorsAlreadyOpen() + 1);
        SomethingThatWaitsHero somethingThatWaitsHero = arena.getListSomethingThatWaitsHero().get(randomDoor);
        if (somethingThatWaitsHero instanceof MagicArtifact) {
            hero.setPower(hero.getPower() + ((MagicArtifact) somethingThatWaitsHero).getPower());
            arena.deleteSomethingThatWaitsHeroById(somethingThatWaitsHero.getId());
        } else if (somethingThatWaitsHero instanceof Monster) {
            if (hero.getPower() >= ((Monster) somethingThatWaitsHero).getPower()) {
                arena.deleteSomethingThatWaitsHeroById(somethingThatWaitsHero.getId());
                return true;
            } else {
                hero.setAlive(false);
                return false;
            }
        }
        return true;
    }

    public static void informationAboutArena() {
        System.out.println(" ______________________________________ ");
        System.out.println("|  DOOR   |    SOMETHING   |    POWER  |");
        System.out.println("|_________|________________|___________|");
        for (SomethingThatWaitsHero something : arena.getListSomethingThatWaitsHero()) {
            System.out.printf(
                    "|  %1$-7s| %2$-15s| %3$5s     |\n", something.getId(), something.getName(), something.getPower()
            );
            System.out.println("|--------------------------------------|");
        }
        System.out.println("");
    }
}
