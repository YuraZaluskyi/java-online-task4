package gameheroandmonster;

public class Monster extends SomethingThatWaitsHero {

    private final int MAX_POWER = 100;
    private final int MIN_POWER = 5;


    public Monster(int id) {
        setId(id);
        setPower(getRandomNumber(MIN_POWER, MAX_POWER));
        setName("MONSTER");
    }

    public int getMAX_POWER() {
        return MAX_POWER;
    }

    public int getMIN_POWER() {
        return MIN_POWER;
    }

    @Override
    public String toString() {
        return "id = " + getId() + "  Monster - " +
                "power = " + getPower() +
                ' ' + '\n';
    }
}
