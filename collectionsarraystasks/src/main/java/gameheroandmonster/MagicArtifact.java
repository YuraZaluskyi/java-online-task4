package gameheroandmonster;

public class MagicArtifact extends SomethingThatWaitsHero {

    private final int MAX_POWER = 80;
    private final int MIN_POWER = 10;

    public MagicArtifact(int id) {
        setId(id);
        setPower(getRandomNumber(MIN_POWER, MAX_POWER));
        setName("MAGIC ARTIFACT");
    }

    public int getMAX_POWER() {
        return MAX_POWER;
    }

    public int getMIN_POWER() {
        return MIN_POWER;
    }

    @Override
    public String toString() {
        return "id = " + getId() + "  MagicArtifact - " +
                "power = " + getPower() +
                ' ' + '\n';
    }
}
