package gameheroandmonster;

public class Hero {

    private final String NAME_HERO = "HERO";
    private boolean isAlive = true;
    private int power = 25;

    public Hero() {
    }

    public String getNAME_HERO() {
        return NAME_HERO;
    }

    public int getPower() {
        return power;
    }

    public void setPower(int power) {
        this.power = power;
    }

    public boolean isAlive() {
        return isAlive;
    }

    public void setAlive(boolean alive) {
        isAlive = alive;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Hero hero = (Hero) o;

        return power == hero.power;
    }

    @Override
    public int hashCode() {
        return power;
    }

    @Override
    public String toString() {
        return "Hero{" +
                "power=" + power +
                '}';
    }

}
