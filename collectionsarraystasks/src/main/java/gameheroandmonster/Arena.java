package gameheroandmonster;

import java.util.ArrayList;
import java.util.List;
import static gameheroandmonster.SomethingThatWaitsHero.getRandomNumber;

public class Arena {
    private final int QUANTITY_DOORS = 10;
    private int quantityDoorsAlreadyOpen = 0;
    private List<SomethingThatWaitsHero> listSomethingThatWaitsHero = new ArrayList<SomethingThatWaitsHero>(QUANTITY_DOORS);

    public Arena() {
        setListWithSomethingThatWaitsHero();
    }

    private List<SomethingThatWaitsHero> setListWithSomethingThatWaitsHero() {
        for (int i = 0; i < QUANTITY_DOORS; i++) {
            int randomNumber = getRandomNumber(1, 2);
            if (randomNumber == 1) {
                listSomethingThatWaitsHero.add(new MagicArtifact(i));
            } else {
                listSomethingThatWaitsHero.add(new Monster(i));
            }
        }
        return listSomethingThatWaitsHero;
    }

    public SomethingThatWaitsHero getSomethingThatWaitsHeroById(int id) {
        for (SomethingThatWaitsHero somethingThatWaitsHero : listSomethingThatWaitsHero) {
            if (somethingThatWaitsHero.getId() == id) {
                return somethingThatWaitsHero;
            }
        }
        return null;
    }

    public boolean isListSomethingThatWaitsHeroEmpty() {
        if (getListSomethingThatWaitsHero().size() == 0) {
            return true;
        } else {
            return false;
        }
    }

    public void deleteSomethingThatWaitsHeroById(int id) {
        getListSomethingThatWaitsHero().remove(getSomethingThatWaitsHeroById(id));

    }

    public int getQuantityDoorsAlreadyOpen() {
        return quantityDoorsAlreadyOpen;
    }

    public void setQuantityDoorsAlreadyOpen(int quantityDoorsAlreadyOpen) {
        this.quantityDoorsAlreadyOpen = quantityDoorsAlreadyOpen;
    }

    public int getQUANTITY_DOORS() {
        return QUANTITY_DOORS;
    }

    public List<SomethingThatWaitsHero> getListSomethingThatWaitsHero() {
        return listSomethingThatWaitsHero;
    }

    public void setListSomethingThatWaitsHero(List<SomethingThatWaitsHero> listSomethingThatWaitsHero) {
        this.listSomethingThatWaitsHero = listSomethingThatWaitsHero;
    }

    @Override
    public String toString() {
        return "Arena{" +
                "QUANTITY_DOORS=" + QUANTITY_DOORS +
                ", listSomethingThatWaitsHero=" + listSomethingThatWaitsHero +
                '}';
    }
}
