package logicaltasksforarrays.seriesIdenticalElements;

/*
* Знайти в масиві всі серії однакових елементів, які йдуть підряд, і видалити з
них всі елементи крім одного.
* */

import java.util.Arrays;

import static logicaltasksforarrays.numbersrepeatedmorethantwiceinarray.Main.*;
import static logicaltasksforarrays.taskaboutwoarrays.Main.*;

public class Main {

    private static int[] array = {1, 1, 12, 3, 3, 4, 5, 1, 5, 15};

    public static void main(String[] args) {

        System.out.println(Arrays.toString(getArrayWithOnlyOneElementInSeries(array)));
    }

    public static int[] getArrayWithOnlyOneElementInSeries(int[] array) {
        while (isAtLeastOnePairIdenticalElementsInArray(array)) {
            int quantity = getQuantityElementsInSeries(array);
            int start = getIndexStartSeries(array);
            array = deleteSeriesElementsFromArrayExceptOne(array, start, quantity);
        }
        return array;
    }

    public static int getQuantityElementsInSeries(int[] array) {
        int counter = 1;
        for (int i = 1; i < array.length; i++) {
            if (array[i] == array[i - 1]) {
                counter++;
            } else if (counter == 1) {
                continue;
            } else {
                return counter;
            }
        }
        return counter;
    }

    public static int getIndexStartSeries(int[] array) {
        for (int i = 1; i < array.length; i++) {
            if (array[i] == array[i - 1]) {
                return i - 1;
            }
        }
        return 0;
    }

    public static int[] deleteSeriesElementsFromArrayExceptOne(int[] array, int start, int quantity) {
        quantity = getQuantityElementsInSeries(array) - 1;
        start = getIndexStartSeries(array) + 1;
        return cutSubArrayFromArray(array, start, quantity);
    }
}
