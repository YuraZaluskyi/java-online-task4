package logicaltasksforarrays.taskaboutwoarrays;

/*
* Дано два масиви. Сформувати третій масив, що складається з тих елементів,
які: а) присутні в обох масивах; б) присутні тільки в одному з масивів.
* */

import java.util.Arrays;

public class Main {

    public static int[] firstArray = {30, 40, 40, 50, 50};
    public static int[] secondArray = {11, 28, 20, 25, 30, 45, 50};


    public static void main(String[] args) {
        sortArray(firstArray);
        sortArray(secondArray);

        System.out.print("first arrayNumbers - ");
        System.out.println(Arrays.toString(firstArray));
        System.out.println("");
        System.out.print("second arrayNumbers - ");
        System.out.println(Arrays.toString(secondArray));
        System.out.println("");
        System.out.print("internal section of arrays - ");
        System.out.println(Arrays.toString(getInternalSectionOfArrays(firstArray, secondArray)));
        System.out.println("");
        System.out.print("outer section of arrays - ");
        System.out.println(Arrays.toString(getOuterSectionOfArrays(firstArray, secondArray)));

    }

    public static int[] getInternalSectionOfArrays(int[] firstArray, int[] secondArray) {
        int[] internalSectionOfArrays;
        sortArray(firstArray);
        sortArray(secondArray);
        if (
                (secondArray[secondArray.length - 1] < firstArray[0]) ||
                        firstArray[firstArray.length - 1] < secondArray[0]
        ) {
            internalSectionOfArrays = new int[0];
        } else {
            int[] connectedArrays = getCombinedArraysSortWithoutIdenticalElements(firstArray, secondArray);
            internalSectionOfArrays = getArrayElementsThatPairedInArray(connectedArrays);
        }
        return internalSectionOfArrays;
    }

    public static int[] getOuterSectionOfArrays(int[] firstArray, int[] secondArray) {
        int[] outerSectionOfArrays;
        sortArray(firstArray);
        sortArray(secondArray);
        if (
                (secondArray[secondArray.length - 1] < firstArray[0]) ||
                        firstArray[firstArray.length - 1] < secondArray[0]
        ) {
            outerSectionOfArrays = getCombinedArraysSortWithoutIdenticalElements(firstArray, secondArray);
        } else {
            int[] connectedArrays = getCombinedArraysSortWithoutIdenticalElements(firstArray, secondArray);
            outerSectionOfArrays = getArrayElementsWithoutPairedInArray(connectedArrays);
        }
        return outerSectionOfArrays;
    }

    public static int[] getCombinedArraysSortWithoutIdenticalElements(int[] firstArray, int[] secondArray) {
        if (isAtLeastOnePairIdenticalElementsInArray(firstArray)) {
            firstArray = getArrayWithoutIdenticalElements(firstArray);
        }
        if (isAtLeastOnePairIdenticalElementsInArray(secondArray)) {
            secondArray = getArrayWithoutIdenticalElements(secondArray);
        }
        int[] connectedArray = getCombineTwoArrays(firstArray, secondArray);
        sortArray(connectedArray);
        return connectedArray;
    }


    public static int[] getCombineTwoArrays(int[] firstArray, int[] secondArray) {
        int[] combinedArray = new int[firstArray.length + secondArray.length];
        for (int i = 0; i < combinedArray.length; i++) {
            if (i < firstArray.length) {
                combinedArray[i] = firstArray[i];
            } else {
                combinedArray[i] = secondArray[i - firstArray.length];
            }
        }
        return combinedArray;
    }

    public static int[] getArrayWithoutIdenticalElements(int[] array) {
        int sizeArray = getSizeOfArrayWithoutIdenticalElements(array);
        int[] arrayUniqueElements = new int[sizeArray];
        arrayUniqueElements[0] = array[0];
        int j = 0;
        for (int i = 1; i < array.length; i++) {
            if (arrayUniqueElements[j] == array[i]) {
                continue;
            }
            j++;
            arrayUniqueElements[j] = array[i];
        }
        return arrayUniqueElements;
    }

    public static int getSizeOfArrayWithoutIdenticalElements(int[] array) {
        int counter = 1;
        int elementSeries = array[0];
        for (int i = 1; i < array.length; i++) {
            if (elementSeries == array[i]) {
                continue;
            }
            elementSeries = array[i];
            counter++;
        }
        return counter;
    }

    /*
     * an arrayNumbers can only have a pair of identical element
     * */
    public static int getQuantityIdenticalPairsOfElementsInArray(int[] arrays) {
        int i = 0;
        int counter = 0;
        while (i < arrays.length - 1) {
            if (arrays[i] == arrays[i + 1]) {
                counter++;
            }
            i++;
        }
        return counter;
    }

    public static int getQuantityElementsWithoutPairsInArray(int[] array) {
        return array.length - (getQuantityIdenticalPairsOfElementsInArray(array) * 2);
    }

    public static int[] getArrayElementsThatPairedInArray(int[] array) {
        int sizeArray = getQuantityIdenticalPairsOfElementsInArray(array);
        int[] arrayElementsThatPairedInArray = new int[sizeArray];
        int j = 0;
        for (int i = 0; i < array.length - 1; i++) {
            if (array[i] == array[i + 1]) {
                arrayElementsThatPairedInArray[j] = array[i];
                j++;
            }
        }
        return arrayElementsThatPairedInArray;
    }

    public static int[] getArrayElementsWithoutPairedInArray(int[] array) {
        int sizeArray = getQuantityElementsWithoutPairsInArray(array);
        int[] arrayElementsWithoutPairedInArray = new int[sizeArray];
        int j = 0;
        int i = 0;
        while (i < array.length - 1) {
            if (array[i] == array[i + 1]) {
                i += 2;
            } else {
                arrayElementsWithoutPairedInArray[j] = array[i];
                j++;
                i++;
            }
            if (i == array.length - 1) {
                if (array[i] != array[i - 1]) {
                    arrayElementsWithoutPairedInArray[j] = array[i];
                }
            }
        }
        return arrayElementsWithoutPairedInArray;
    }

    public static boolean isAtLeastOnePairIdenticalElementsInArray(int[] array) {
        int i = 0;
        while (i < array.length - 1) {
            if (array[i] == array[i + 1]) {
                return true;
            }
            i++;
        }
        return false;
    }

    public static void sortArray(int[] array) {
        int temporaryValue = array[0];
        for (int i = 0; i < array.length; i++) {
            for (int j = i + 1; j < array.length; j++) {
                if (array[j] < array[i]) {
                    temporaryValue = array[j];
                    array[j] = array[i];
                    array[i] = temporaryValue;
                }
            }
        }
    }
}
