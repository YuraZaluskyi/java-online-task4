package logicaltasksforarrays.numbersrepeatedmorethantwiceinarray;

import java.util.Arrays;

import static logicaltasksforarrays.taskaboutwoarrays.Main.*;

/*
 * Видалити в масиві всі числа, які повторюються більше двох разів.
 * */
public class Main {

    public static int[] arrayNumbers = {10, 20, 20, 20, 25, 30, 30, 30, 40, 40, 40, 45};

    public static void main(String[] args) {
        Arrays.sort(arrayNumbers);
        System.out.println(Arrays.toString(arrayNumbers));
        System.out.println(Arrays.toString(deleteAllNumberRepeatedMoreTwiceFromArray(arrayNumbers)));
    }

    public static int[] deleteAllNumberRepeatedMoreTwiceFromArray(int[] array) {
        Arrays.sort(array);
        if ((!isNumberRepeatedMoreTwice(array)) || (array.length < 3)) {
            return new int[0];
        }
        int i = 0;
        while (i < array.length - 2) {
            int quantity = getNumberTimesElementIsInArray(array, array[i]);
            if (quantity > 2) {
                array = cutSubArrayFromArray(array, i, quantity);
                Arrays.sort(array);
            } else {
                i++;
            }
        }
        return array;
    }

    public static boolean isNumberRepeatedMoreTwice(int[] array) {
        for (int i = 1; i < array.length - 1; i++) {
            if ((array[i] == array[i - 1]) && (array[i] == array[i + 1])) {
                return true;
            }
        }
        return false;
    }

    public static int getNumberTimesElementIsInArray(int[] array, int number) {
        int counter = 0;
        for (int i = 0; i < array.length; i++) {
            if (array[i] == number) {
                counter++;
            }
        }
        return counter;
    }

    public static int[] cutSubArrayFromArray(int[] array, int start, int quantityElements) {
        int[] firstArray = new int[start];
        int[] secondArray = new int[array.length - start - quantityElements];
        for (int i = 0; i < firstArray.length; i++) {
            firstArray[i] = array[i];
        }
        for (int i = 0; i < secondArray.length; i++) {
            secondArray[i] = array[start + quantityElements + i];
        }
        return getCombineTwoArrays(firstArray, secondArray);
    }

}
